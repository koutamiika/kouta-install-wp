@echo off

echo.
echo.
echo ##   ##   #####   ##   ##  ######    ##### 
echo ##   ##  ##   ##  ##   ##    ##     ##   ##
echo ##  ##   ##   ##  ##   ##    ##     ##   ##
echo #####    ##   ##  ##   ##    ##     #######
echo ##  ##   ##   ##  ##   ##    ##     ##   ##
echo ##   ##  ##   ##  ##   ##    ##     ##   ##
echo ##   ##   #####    #####     ##     ##   ##
echo.
echo --------------------------------------------------------------------------------
echo.
echo This script installs latest WordPress, creates database and cleans up default installation.
echo Before continuing, set your wamp/xamp www root and database user credentials.
echo.
echo NOTICE: This script requires WP-CLI
echo https://wp-cli.org/
echo.
echo --------------------------------------------------------------------------------
echo.

pause

SET ROOT=C:\wamp\www
SET DBUSER=root
SET DBPASS=

SET /p site_name="Project name (lowercase only, no special characters other than dash allowed): "

:: Was an argument passed?
IF [%site_name%] == [] GOTO fatal

SET /p USER="User name: "

IF [%USER%] == [] GOTO fatal

SET /p PASS="Password: "

IF [%PASS%] == [] GOTO fatal

SET /p EMAIL="Email: "

IF [%EMAIL%] == [] GOTO fatal


CD %ROOT%
mkdir %site_name%
CD %site_name%

echo.
echo --------------------------------------------------------------------------------
echo Installing WordPress..
echo.

:: Download WP Core files
call wp core download --locale=fi

:: Create wp-config.php
call wp config create --dbname=%site_name% --dbuser=%DBUSER% --locale=fi

:: Create database with database with specified in wp-config.php
call wp db create

:: Run WordPress installation
call wp core install --url="http://localhost/%site_name%" --title="%site_name%" --admin_user=%USER% --admin_password=%PASS% --admin_email=%EMAIL%

echo.
echo --------------------------------------------------------------------------------
echo Installing plugins
echo.

call wp plugin install c:/wamp/www/advanced-custom-fields-pro.zip --activate
call wp plugin install contact-form-7 --activate
call wp plugin install flamingo --activate
call wp plugin install all-in-one-wp-migration --activate
call wp plugin install https://bitbucket.org/koutamiika/kouta-blocks/get/09b78ff7b110.zip

echo.
echo --------------------------------------------------------------------------------
echo Removing default plugins
echo.

call wp plugin uninstall akismet hello.php

echo.
echo --------------------------------------------------------------------------------
echo Setting default options
echo.

:: Discourage search engines
call wp option update blog_public 0

echo.
SET /p theme="Download and activate Koutabase theme? [y/n]: "
if /I "%theme%" EQU "y" goto :theme
if /I "%theme%" EQU "n" goto :notheme

:theme
echo.
echo --------------------------------------------------------------------------------
echo Installing Koutabase theme
echo.

:: Installing koutabase theme from Bitbucket
CD wp-content\themes
call git clone https://koutamiika@bitbucket.org/koutamiika/koutabase.git
call rename koutabase %site_name%

echo.
echo --------------------------------------------------------------------------------
echo Installing theme dependencies
echo.

CD %site_name%
call npm install gulp@3.9.1 
call npm install gulp-compass --save-dev 
call npm i browser-sync --save 
call npm install --save wp-cli

call wp theme activate %site_name%

:notheme
:: Clear Window
cls
    
echo.
echo.
echo.
echo ##   ##   #####   ##   ##  ######    ##### 
echo ##   ##  ##   ##  ##   ##    ##     ##   ##
echo ##  ##   ##   ##  ##   ##    ##     ##   ##
echo #####    ##   ##  ##   ##    ##     #######
echo ##  ##   ##   ##  ##   ##    ##     ##   ##
echo ##   ##  ##   ##  ##   ##    ##     ##   ##
echo ##   ##   #####    #####     ##     ##   ##
echo.
echo --------------------------------------------------------------------------------
echo Installation is complete. Your username/password have been added to your clipboard and are listed below.
echo.
echo Username: %USER%
echo Password: %PASS%
echo.

echo Happy coding :)

echo If you installed Koutabase theme, you can start developing by running "gulp"

echo.

:: Add to clipboard
echo Username: %USER% Password: %PASS% | clip
    
:: start the project in browser
start http://localhost/%site_name%

call code .

pause

goto:eof

:fatal
echo.
echo --------------------------------------------------------------------------------
echo Required information missing. Please try again.
echo.

pause

goto:eof
    
:END